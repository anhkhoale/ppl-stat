import React, { useEffect, useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

function Login() {
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState('');

  async function login() {
    const res = await fetch(`http://localhost:8080/api/login/?username=${username}?password=${password}`);
    navigate(`http://localhost:3000/dashboard/?username=${res.username}`);
  }

  return (
    <Container style={{ width: '20%', marginTop: '20px' }}>
      <Form className="center-form border border-secondary p-3 mt-20 ml-20">
        <h2>Login</h2>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Username: </Form.Label>
          <Form.Control className="w-20" placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)} />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={login}>
          Login
        </Button>
      </Form>
    </Container>
  );
}

export default Login;
