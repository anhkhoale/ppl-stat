import { Container, Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Navigation() {
  return (
    <header className="App-header">
      <Navbar bg="light" className="mb-3">
        <Container>
          <Navbar.Brand as={Link} to="/">
            People Stats
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/search">
              Search
            </Nav.Link>
            <Nav.Link as={Link} to="/api/user/">
              Dashboard
            </Nav.Link>
          </Nav>
          <Nav className="mx-3">
            <Nav.Link as={Link} to="/login">
              Sign in
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </header>
  );
}

export default Navigation;
