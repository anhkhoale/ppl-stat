import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { QueryClient, QueryClientProvider } from 'react-query';
import Login from './components/Login';
import Search from './components/Search';
import Home from './components/Home';
import Dashboard from './components/Dashboard';

const queryClient = new QueryClient();

function Routing() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/search" element={<Search />} />
          <Route path="/api/user/:username" element={<Dashboard />} />
        </Routes>
      </Router>
    </QueryClientProvider>
  );
}
ReactDOM.render(
  <React.StrictMode>
    <Routing />
  </React.StrictMode>,
  document.getElementById('root'),
);
