const fs = require('fs');
const axios = require('axios');

const generateData = async (users) => {
  const { data } = await axios.get('https://randomuser.me/api/', {
    params: {
      results: users,
    },
  });

  fs.writeFileSync('database.json', JSON.stringify(data, null, 4));
};

generateData(5000);
