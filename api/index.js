const express = require('express');
const crypto = require('crypto');
const data = require('../database.json');

const app = express();

app.listen(8080, () => {
  console.log('Api is running on port 8080');
});

app.get('/api/login/', (req, res) => {
  const { username } = req.query;
  const { password } = req.query;
  if (!username || !password) {
    return res.json({ message: 'Connection failed' });
  }

  const user = data.results.filter((obj) => obj.login.username === username);

  if (!user[0]) {
    return res.json({ message: 'User not found' });
  }
  const { salt } = user[0].login;
  const key = password + salt;
  const crypt = crypto.createHash('sha256').update(key).digest('hex');

  if (user[0].login.sha256 !== crypt) {
    return res.json({ message: 'Wrong password' });
  }

  res.json({ user });
});

app.get('/api/dataLength', (req, res) => {
  res.json({ data: data.results.length });
});

app.get('api/user/', (req, res) => {
  const { username } = req.query;
  const result = data.results.filter(
    (obj) => obj.login.username === username,
  );

  res.json({ result });
});

app.get('/api/countriesReq', (req, res) => {
  const country = [...new Set(data.results.map((item) => item.location.country))];
  const numberForEachCountry = [];
  let result = [];
  for (let index = 0; index < country.length; index++) {
    const element = country[index];

    numberForEachCountry.push(
      data.results.filter((obj) => obj.location.country === element).length,
    );

    result = numberForEachCountry.map((e) => parseFloat((e / 5000) * 100).toFixed(2));
  }

  const parsedResult = {};

  for (let i = 0; i < country.length; i++) {
    parsedResult[country[i]] = result[i];
  }

  res.json({ data: parsedResult });
});

app.get('/api/gender', (req, res) => {
  const female = data.results.filter((obj) => obj.gender === 'female').length;
  const male = data.results.filter((obj) => obj.gender === 'male').length;

  res.json({ female: (100 * female) / 5000, male: (100 * male) / 5000 });
});

app.get('/api/searchUser/', async (req, res) => {
  let response = data.results;

  if (req.query.firstname && req.query.firstname.length > 0) {
    response = response.filter(
      (obj) => req.query.firstname && req.query.firstname === obj.name.first,
    );
  }
  if (req.query.lastname && req.query.lastname.length > 0) {
    response = response.filter(
      (obj) => req.query.lastname && req.query.lastname === obj.name.last,
    );
  }
  if (req.query.gender && req.query.gender.length > 0) {
    response = response.filter(
      (obj) => req.query.gender && req.query.gender === obj.gender,
    );
  }
  if (req.query.state && req.query.state.length > 0) {
    response = response.filter(
      (obj) => req.query.state && req.query.state === obj.location.state,
    );
  }
  if (req.query.city && req.query.city.length > 0) {
    response = response.filter(
      (obj) => req.query.city && req.query.city === obj.location.city,
    );
  }
  if (req.query.phone && req.query.phone.length > 0) {
    response = response.filter(
      (obj) => req.query.phone && req.query.phone === obj.phone,
    );
  }
  res.json({ response });
});
